"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsx_runtime_1 = require("react/jsx-runtime");
const react_1 = require("@testing-library/react");
const user_event_1 = __importDefault(require("@testing-library/user-event"));
const SelectMenu_1 = __importDefault(require("./SelectMenu"));
test('shows label and options', () => __awaiter(void 0, void 0, void 0, function* () {
    const label = 'hello';
    const options = [
        { id: 1, name: 'Apple' },
        { id: 2, name: 'Banana' },
    ];
    (0, react_1.render)((0, jsx_runtime_1.jsx)(SelectMenu_1.default, { label: label, options: options }));
    expect(react_1.screen.getByLabelText(label)).toBeInTheDocument();
    const selectMenuInputBox = react_1.screen.getByRole('button');
    user_event_1.default.click(selectMenuInputBox);
    const foundOptions = yield react_1.screen.findAllByRole('option');
    foundOptions.forEach((option, index) => {
        expect(option).toHaveTextContent(options[index].name);
    });
}));
test('can select option', () => __awaiter(void 0, void 0, void 0, function* () {
    const label = 'hello';
    const options = [
        { id: 1, name: 'Apple' },
        { id: 2, name: 'Banana' },
    ];
    (0, react_1.render)((0, jsx_runtime_1.jsx)(SelectMenu_1.default, { label: label, options: options }));
    const selectMenuInputBox = react_1.screen.getByRole('button');
    user_event_1.default.click(selectMenuInputBox);
    const firstOption = yield react_1.screen.findByRole('option', {
        name: options[0].name,
    });
    user_event_1.default.click(firstOption);
    expect(selectMenuInputBox).toHaveTextContent('Apple');
}));
//# sourceMappingURL=SelectMenu.spec.js.map