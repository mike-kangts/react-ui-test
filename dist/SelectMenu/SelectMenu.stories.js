"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SelectMenuWithLabel = void 0;
const jsx_runtime_1 = require("react/jsx-runtime");
const SelectMenu_1 = __importDefault(require("./SelectMenu"));
// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
exports.default = {
    title: 'SelectMenu',
    component: SelectMenu_1.default,
};
const Template = (args) => (0, jsx_runtime_1.jsx)(SelectMenu_1.default, Object.assign({}, args));
exports.SelectMenuWithLabel = Template.bind({});
exports.SelectMenuWithLabel.args = {
    label: 'Assign To',
    options: [
        { id: 1, name: 'Wade Cooper' },
        { id: 2, name: 'Arlene Mccoy' },
        { id: 3, name: 'Devon Webb' },
        { id: 4, name: 'Tom Cook' },
        { id: 5, name: 'Tanya Fox' },
        { id: 6, name: 'Hellen Schmidt' },
        { id: 7, name: 'Caroline Schultz' },
        { id: 8, name: 'Mason Heaney' },
        { id: 9, name: 'Claudie Smitham' },
        { id: 10, name: 'Emil Schaefer' },
    ],
};
//# sourceMappingURL=SelectMenu.stories.js.map