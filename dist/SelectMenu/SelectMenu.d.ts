import { FC } from 'react';
interface Option {
    id?: number;
    name: string;
}
export interface SelectMenuProps {
    label: string;
    options: Option[];
}
declare const SelectMenu: FC<SelectMenuProps>;
export default SelectMenu;
