import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import SelectMenu from './SelectMenu';

test('shows label and options', async () => {
    const label = 'hello';
    const options = [
        { id: 1, name: 'Apple' },
        { id: 2, name: 'Banana' },
    ];
    render(<SelectMenu label={label} options={options} />);

    expect(screen.getByLabelText(label)).toBeInTheDocument();

    const selectMenuInputBox = screen.getByRole('button');
    userEvent.click(selectMenuInputBox);

    const foundOptions = await screen.findAllByRole('option');

    foundOptions.forEach((option, index) => {
        expect(option).toHaveTextContent(options[index].name);
    });
});

test('can select option', async () => {
    const label = 'hello';
    const options = [
        { id: 1, name: 'Apple' },
        { id: 2, name: 'Banana' },
    ];
    render(<SelectMenu label={label} options={options} />);

    const selectMenuInputBox = screen.getByRole('button');
    userEvent.click(selectMenuInputBox);
    const firstOption = await screen.findByRole('option', {
        name: options[0].name,
    });

    userEvent.click(firstOption);
    expect(selectMenuInputBox).toHaveTextContent('Apple');
});
