module.exports = {
    content: ['./public/**/*.html', './src/**/*.{js,jsx,ts,tsx}'],
    theme: {},
    plugins: [
        require("tailwindcss-scoped-groups")({
            groups: ["one", "two"],
        }),
    ],
};
